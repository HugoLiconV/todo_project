package android.eps.uam.es.todo_project;

import android.app.DatePickerDialog;
import android.eps.uam.es.todo_project.models.RealmLocalTask;
import android.eps.uam.es.todo_project.models.Task;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import io.realm.Realm;
import io.realm.RealmResults;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddTask extends AppCompatActivity {

  public static final String TASK = "TASK";
  Toolbar myToolbar;
  private ActionMenuView amvMenu;
  private Task mTask;
  boolean mIsNewTask;
  boolean mIsEditing = false;


  private TextView lblDateAdd, lblPriorityAdd;
  private EditText txtTitleAdd, txtDescriptionAdd, txtTimeAdd;
  //private DataBaseService dataBaseService;
  private Realm realm;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    boolean isPreviewMode;
    mTask = getModel();
    isPreviewMode = isPreviewTheCurrentMode(mTask);
    initializeViews();
    populateFields(mTask);

    createPopUpMenu();
    initToolbar();
    switchMode(isPreviewMode);
//    dataBaseService = DataBaseService.getDatabaseService();
    realm = Realm.getDefaultInstance();
  }

  /**
   * Metodo para habilitar o deshabilitar los elementos,
   * este dependiendo si se esta creando una nueva tarea
   * o se está editando una ya existente
   * @param isPreviewMode boolean para identificar si esta en modo
   * previsualizacion o no, si esta en modo previsualizacion los elementos
   * quedarán deshabilitados
   */
  private void switchMode(boolean isPreviewMode) {
    txtTitleAdd.setEnabled(!isPreviewMode);
    txtDescriptionAdd.setEnabled(!isPreviewMode);
    lblDateAdd.setEnabled(!isPreviewMode);
    txtTimeAdd.setEnabled(!isPreviewMode);
    lblPriorityAdd.setEnabled(!isPreviewMode);
  }

  /**
   * Metodo para crear el popUp Menu que permite seleccionar la prioridad
   */
  private void createPopUpMenu() {
    lblPriorityAdd.setOnClickListener(v -> {
      PopupMenu popupMenu = new PopupMenu(AddTask.this, lblPriorityAdd);
/*//      inicio
      try {
        Field[] fields = popupMenu.getClass().getDeclaredFields();
        for (Field field : fields) {
          if ("mPopup".equals(field.getName())) {
            field.setAccessible(true);
            Object menuPopupHelper = field.get(popupMenu);
            Class<?> classPopupHelper = Class.forName(menuPopupHelper
                .getClass().getName());
            Method setForceIcons = classPopupHelper.getMethod(
                "setForceShowIcon", boolean.class);
            setForceIcons.invoke(menuPopupHelper, true);
            break;
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
//      Fin*/
      popupMenu.getMenuInflater().inflate(R.menu.popup_priority, popupMenu.getMenu());
      popupMenu.setOnMenuItemClickListener(item -> {
        Toast.makeText(AddTask.this, "" + item.getTitle(), Toast.LENGTH_SHORT).show();
        lblPriorityAdd.setText(item.getTitle());
        return true;
      });
      popupMenu.show();
    });
  }

  /***
   * Inicializa los campos de la Actividad,
   * ya sea si es una nueva tarea o una existente
   * @param task Objeto tipo tarea para obtener los datos de esta
   * y colocarlos en los campos de la actividad
   */
  private void populateFields(Task task) {
    txtTitleAdd.setText(task.getTodoTitle());
    txtDescriptionAdd.setText(task.getTodoDescription());
    if (mIsNewTask) {
      java.util.Calendar cal = java.util.Calendar.getInstance();
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
      lblDateAdd.setText(sdf.format(cal.getTime()));
    } else {
      lblDateAdd.setText(new SimpleDateFormat("dd/MMMM/yyyy").format(task.getTodoDate()));
    }
    txtTimeAdd.setText(task.getTodoTime() + "");
    lblPriorityAdd.setText(task.getTodoPriority() + "");
  }

  /**
   * Inicialza los elementos del Activity
   */
  private void initializeViews() {
    setContentView(R.layout.activity_add_task);
    myToolbar = (Toolbar) findViewById(R.id.toolbar);
    txtTitleAdd = (EditText) findViewById(R.id.txtTitleAdd);
    txtDescriptionAdd = (EditText) findViewById(R.id.txtDescriptionAdd);
    txtTimeAdd = (EditText) findViewById(R.id.txtTimeAdd);
    lblDateAdd = (TextView) findViewById(R.id.lblDateAdd);
    lblPriorityAdd = (TextView) findViewById(R.id.lblPriorityAdd);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    amvMenu = (ActionMenuView) myToolbar.findViewById(R.id.amvMenu);
    amvMenu.setOnMenuItemClickListener(this::onOptionsItemSelected);
  }

  /**
   * Metodo para saber si esta en modo preview, es decir si se
   * está mostrando una tarea ya existente.
   * @param task Objeto tipo tarea
   * @return true si esta en modo preview
   */
  private boolean isPreviewTheCurrentMode(Task task) {
    return !task.isEmpty();
  }

  /**
   * Obtiene el modelo de la tarea
   * @return Objeto de tipo tarea
   */
  private Task getModel() {
    Bundle extras = getIntent().getExtras();
    Task task;
    if (extras == null) {
      task = new Task();
      mIsNewTask = true;
    } else {
      task = extras.getParcelable(TASK);
      mIsNewTask = false;
    }
    return task;
  }

  /**
   * Inicializa el toolbar de la activit7
   */
  private void initToolbar() {
    setSupportActionBar(myToolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

  }

  /**
   * Método para agregar las opciones al appbar
   */
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    //getMenuInflater().inflate(R.menu.navigation_main_activity, menu);
    MenuInflater inflater = getMenuInflater();
    if (mIsNewTask) {
      inflater.inflate(R.menu.navigation_add_task, menu);
    } else {
      inflater.inflate(R.menu.navigation_edit_task, menu);
    }
    return super.onCreateOptionsMenu(menu);
  }

  /**
   * Método para seleccionar los eventos del appbar
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.navigation_done:
        mIsEditing = false;
        invalidateOptionsMenu();
        getTaskData();
        saveTask(mTask, mIsNewTask);
        finish();
        return true;
      case R.id.navigation_edit:
        mIsEditing = true;
        switchMode(false);
        invalidateOptionsMenu();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }



  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    if (mIsEditing || mIsNewTask) {
      menu.findItem(R.id.navigation_done).setVisible(true);
      menu.findItem(R.id.navigation_edit).setVisible(false);
    } else {
      menu.findItem(R.id.navigation_done).setVisible(false);
      menu.findItem(R.id.navigation_edit).setVisible(true);
    }
    return super.onPrepareOptionsMenu(menu);
  }


  /**
   * Metodo para guardar una tarea
   * @param task Tarea a guardar
   * @param isNewTask Boolean para conocer si es una nueva tarea
   * o es una ya existente.
   */
  private void saveTask(Task task, boolean isNewTask) {
    if (isNewTask) {
      realm.executeTransaction(realm -> {
        Number currentIdNum = realm.where(RealmLocalTask.class).max("id");
        long nextId;
        if (currentIdNum == null) {
          nextId = 1;
        } else {
          nextId = currentIdNum.intValue() + 1;
        }
        task.setTodoIdentifier(nextId);
        task.setTodoCreated(new Date());
        RealmLocalTask localTask = new RealmLocalTask(task);
        realm.copyToRealmOrUpdate(localTask);
      });
    }else{
      //getTaskData();
      realm.executeTransaction(realm -> {
        RealmLocalTask localTask = new RealmLocalTask(task);
        realm.copyToRealmOrUpdate(localTask);
      });
    }
  }

  /**
   * Metodo para obtener los datos de los campos de la Actividad
   */
  private void getTaskData() {
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    mTask.setTodoTitle(String.valueOf(txtTitleAdd.getText()));
    mTask.setTodoDescription(String.valueOf(txtDescriptionAdd.getText().toString()));
    try {
      mTask.setTodoDate((formatter.parse(lblDateAdd.getText().toString())));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    mTask.setTodoTime(Integer.parseInt(txtTimeAdd.getText().toString().trim()));
    mTask.setTodoPriority(Integer.parseInt(lblPriorityAdd.getText().toString().trim()));
  }

  /**
   * Metodo para mostrar el date Picker
   * @param view
   */
  public void showDatePickerDialog(View view) {
    final java.util.Calendar calendar = java.util.Calendar.getInstance();

    int year = calendar.get(java.util.Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(java.util.Calendar.DAY_OF_MONTH);

    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
        (v, _year, monthOfYear, dayOfMonth) -> lblDateAdd
            .setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + _year), year, month, day);
    datePickerDialog.show();
  }


}
