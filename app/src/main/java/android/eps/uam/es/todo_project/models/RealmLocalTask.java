package android.eps.uam.es.todo_project.models;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Hugo on 21/05/17.
 */

public class RealmLocalTask extends RealmObject {

  @PrimaryKey
  private long id;
  private String todoTitle;
  private String todoDescription;
  private Date todoDate;
  private int todoTime;
  private int todoPriority;
  private boolean isDone;
  private Date todoCreated;
  private boolean isSelected;

  public RealmLocalTask() {

  }

  @Override
  public String toString() {
    return "id: " +id+
        " title: " +todoTitle+
        " description: " +todoDescription+
        " date: " +todoDate+
        " time: " +todoTime+
        " priority: " +todoPriority+
        " is done?: " +isDone+
        " Created: "+ todoCreated+
        " isSelected: " + isSelected +
        "\n";
  }

  public RealmLocalTask(Task task) {
    this.id = task.getTodoIdentifier();
    this.todoTitle = task.getTodoTitle();
    this.todoDescription = task.getTodoDescription();
    this.todoDate = task.getTodoDate();
    this.todoTime = task.getTodoTime();
    this.todoPriority = task.getTodoPriority();
    this.isDone = task.isDone();
    this.todoCreated = task.getTodoCreated();
    this.isSelected = task.isSelected();
  }

  public static List<Task> getTaskList(RealmResults<RealmLocalTask> localTaskList) {
    List<Task> taskList = new ArrayList<>();
    for (RealmLocalTask localTask : localTaskList) {
      taskList.add(
          new Task(localTask.id, localTask.todoTitle, localTask.todoDescription, localTask.todoDate,
              localTask.todoTime, localTask.todoPriority, localTask.isDone, localTask.todoCreated, localTask.isSelected));
    }
    return taskList;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTodoTitle() {
    return todoTitle;
  }

  public void setTodoTitle(String todoTitle) {
    this.todoTitle = todoTitle;
  }

  public String getTodoDescription() {
    return todoDescription;
  }

  public void setTodoDescription(String todoDescription) {
    this.todoDescription = todoDescription;
  }

  public Date getTodoDate() {
    return todoDate;
  }

  public void setTodoDate(Date todoDate) {
    this.todoDate = todoDate;
  }

  public int getTodoTime() {
    return todoTime;
  }

  public void setTodoTime(int todoTime) {
    this.todoTime = todoTime;
  }

  public int getTodoPriority() {
    return todoPriority;
  }

  public void setTodoPriority(int todoPriority) {
    this.todoPriority = todoPriority;
  }

  public boolean isDone() {
    return isDone;
  }

  public void setDone(boolean done) {
    isDone = done;
  }

  public Date getTodoCreated() {
    return todoCreated;
  }

  public void setTodoCreated(Date todoCreated) {
    this.todoCreated = todoCreated;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }
}
