package android.eps.uam.es.todo_project.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.eps.uam.es.todo_project.AddTask;
import android.eps.uam.es.todo_project.MainActivity;
import android.eps.uam.es.todo_project.R;
import android.eps.uam.es.todo_project.local_storage.DataBaseService;
import android.eps.uam.es.todo_project.models.ItemTouchHelperAdapter;
import android.eps.uam.es.todo_project.models.RealmLocalTask;
import android.eps.uam.es.todo_project.models.Task;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import io.realm.Realm;
import io.realm.Realm.Transaction;
import io.realm.RealmResults;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Hugo on 06/05/17.
 */

/**
 * Adaptador para el recyclerView
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

  public static OnReload mParent;
  private List<Task> mTaskList;
  private static DataBaseService mDatabaseService;
  static Realm mRealm;

  public TaskAdapter(Activity parent) {
    mTaskList = new ArrayList<>();
    mDatabaseService = new DataBaseService();
    mParent = (OnReload) parent;
    mRealm = Realm.getDefaultInstance();
  }

  public List<Task> getList() {
    return this.mTaskList;
  }


  /**
   * Interfaz para recargar la lista cada vez que se elimina, actualiza o agrega un dato
   */
  public interface OnReload {
    void onReload();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.todo_list_item, parent, false);
    return new ViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(TaskAdapter.ViewHolder viewHolder, int position) {
    viewHolder.bindData(mTaskList.get(position));
  }

  @Override
  public int getItemCount() {
    return mTaskList.size();
  }

  public void setTaskList(List<Task> taskList) {
    mTaskList = taskList;
  }

  /**
   * Metodo para eliminar un elemento de la base de datos
   * @param id del elemento a borrar dentro de la base de datos
   */
  public static void deletFromDatabase(long id) {
    final RealmResults<RealmLocalTask> results = mRealm
        .where(RealmLocalTask.class)
        .equalTo("id", id)
        .findAll();
    // All changes to data must happen in a transaction
    mRealm.executeTransaction(realm -> {
      results.deleteFromRealm(0);
    });
    mParent.onReload();
  }

  static class ViewHolder extends RecyclerView.ViewHolder {

    private CardView mContainer;
    private TextView mTitle;
    private TextView mDate;
    private TextView mTime;
    private TextView mPriority;
    private Task mTask;
    private CheckBox mIsDone;

    Boolean longClick = false;

    public ViewHolder(View itemView) {
        super(itemView);
        mContainer = (CardView) itemView.findViewById(R.id.cardViewContainer);
        mTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        mDate = (TextView) itemView.findViewById(R.id.txtDate);
        mTime = (TextView) itemView.findViewById(R.id.txtTime);
        mPriority = (TextView) itemView.findViewById(R.id.txtPriority);
        mIsDone = (CheckBox) itemView.findViewById(R.id.isDoneCheckbox);
      }

    void bindData(Task task) {
      this.mTask = task;
      final Context context = itemView.getContext();

      mTitle.setText(task.getTodoTitle());
      mDate.setText(new SimpleDateFormat("dd.MMMM.yyyy").format(task.getTodoDate()));
      mTime.setText(task.getTodoTime() + " m");
      mPriority.setText(task.getTodoPriority() + "");
      mIsDone.setChecked(task.isDone());

      //si el elemento a sido seleccionado por el algoritmo, este debe cambiar el backround
      if (task.isSelected()){
        mContainer.setCardBackgroundColor(itemView.getResources().getColor(R.color.screen3));
      }else{
        mContainer.setCardBackgroundColor(itemView.getResources().getColor(R.color.white));
      }

      /**
       * Listener del checkbox para establecer es estado de la tarea, terminada o no,
       * ademas para realizar el guardado de esta dentro de la base de datos
       */
      mIsDone.setOnCheckedChangeListener((buttonView, isChecked) -> {
        if (isChecked) {
          mRealm.executeTransaction(realm -> {
//            Task tempTask = new Task(task.getTodoIdentifier(),
//                task.getTodoTitle(),
//                task.getTodoDescription(),
//                task.getTodoDate(),
//                task.getTodoTime(),
//                task.getTodoPriority(),
//                isChecked);
            mTask.setDone(isChecked);
            RealmLocalTask localTask = new RealmLocalTask(mTask);
            mRealm.copyToRealmOrUpdate(localTask);
          });
          Toast.makeText(context, "" + task.isDone(), Toast.LENGTH_SHORT).show();
        } else {
          mRealm.executeTransaction(realm -> {
//            task.setDone(true);
//            Task tempTask = new Task(task.getTodoIdentifier(),
//                task.getTodoTitle(),
//                task.getTodoDescription(),
//                task.getTodoDate(),
//                task.getTodoTime(),
//                task.getTodoPriority(),
//                !isChecked);
            mTask.setDone(isChecked);
            RealmLocalTask localTask = new RealmLocalTask(mTask);
            mRealm.copyToRealmOrUpdate(localTask);
          });
          //Toast.makeText(context, "" + task.isDone(), Toast.LENGTH_SHORT).show();
        }
      });

      /**
       * Listener del itemView seleccionado para abir una nueva actividad y poder editar a este
       */
      itemView.setOnClickListener(v -> {
        Intent intent = new Intent(context, AddTask.class);
        Log.e("mTask", mTask.getTodoTitle()+" id: " + mTask.getTodoIdentifier());
        Log.e("Task", task.getTodoTitle()+" id: " + task.getTodoIdentifier());
        intent.putExtra(AddTask.TASK, mTask);
        context.startActivity(intent);
      });
      /**
       * Long click listener que permite eliminar una tarea
       */
      itemView.setOnLongClickListener(
          (v) -> {
            longClick = true;
            v.setOnCreateContextMenuListener((menu, v1, menuInfo) -> {
              menu.setHeaderTitle("Options");
              MenuItem remove = menu.add("Delete");
              remove.setOnMenuItemClickListener(item -> {
                deletFromDatabase(mTask.getTodoIdentifier());
                mParent.onReload();
                return false;
              });
            });
            return false;
          }
      );
    }
  }
}
