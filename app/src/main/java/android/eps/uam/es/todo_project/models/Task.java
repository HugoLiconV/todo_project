package android.eps.uam.es.todo_project.models;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Hugo on 06/05/17.
 */

public class Task implements Parcelable {
  private String todoTitle;
  private String todoDescription;
  private Date todoDate;
  private int todoTime;
  private int todoPriority;
  private long todoIdentifier;
  private boolean isDone;
  private Date todoCreated;
  private boolean isSelected;

  public static final Creator<Task> CREATOR = new Creator<Task>() {
    @Override
    public Task createFromParcel(Parcel in) {
      return new Task(in);
    }

    @Override
    public Task[] newArray(int size) {
      return new Task[size];
    }
  };

  public Task(long todoIdentifier, String todoTitle, String todoDescription, Date todoDate, int todoTime,
      int todoPriority, boolean isDone, Date todoCreated, boolean isSelected) {
    this.todoTitle = todoTitle;
    this.todoDescription = todoDescription;
    this.todoDate = todoDate;
    this.todoTime = todoTime;
    this.todoPriority = todoPriority;
    this.todoIdentifier = todoIdentifier;
    this.isDone = isDone;
    this.todoCreated = todoCreated;
    this.isSelected = isSelected;
  }

  public Task(){
    todoTitle = "";
    todoDescription = "";
    todoDate = null;
    todoTime = 0;
    todoPriority = 1;
    todoIdentifier = 0;
    isDone = false;
    todoCreated = new Date();
    isSelected = false;
  }

  protected Task(Parcel in) {
    todoTitle = in.readString();
    todoDescription = in.readString();
    long tmpTodoDate = in.readLong();
    todoDate = tmpTodoDate != -1 ? new Date(tmpTodoDate) : null;
    todoTime = in.readInt();
    todoPriority = in.readInt();
    todoIdentifier = in.readLong();
    isDone = in.readByte() != 0x00;
    long tmpTodoCreated = in.readLong();
    todoCreated = tmpTodoCreated != -1 ? new Date(tmpTodoCreated) : null;
    isSelected = in.readByte() != 0x00;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(todoTitle);
    dest.writeString(todoDescription);
    dest.writeLong(todoDate != null ? todoDate.getTime() : -1L);
    dest.writeInt(todoTime);
    dest.writeInt(todoPriority);
    dest.writeLong(todoIdentifier);
    dest.writeByte((byte) (isDone ? 0x01 : 0x00));
    dest.writeLong(todoCreated != null ? todoCreated.getTime() : -1L);
    dest.writeByte((byte) (isSelected ? 0x01 : 0x00));
  }

  public boolean isDone() {
    return isDone;
  }

  public void setDone(boolean done) {
    isDone = done;
  }

  public String getTodoTitle() {
    return todoTitle;
  }

  public void setTodoTitle(String todoTitle) {
    this.todoTitle = todoTitle;
  }

  public String getTodoDescription() {
    return todoDescription;
  }

  public void setTodoDescription(String todoDescription) {
    this.todoDescription = todoDescription;
  }

  public Date getTodoDate() {
    return todoDate;
  }

  public void setTodoDate(Date todoDate) {
    this.todoDate = todoDate;
  }

  public int getTodoTime() {
    return todoTime;
  }

  public void setTodoTime(int todoTime) {
    this.todoTime = todoTime;
  }

  public int getTodoPriority() {
    return todoPriority;
  }

  public void setTodoPriority(int todoPriority) {
    this.todoPriority = todoPriority;
  }

  public long getTodoIdentifier() {
    return todoIdentifier;
  }

  public void setTodoIdentifier(long todoIdentifier) {
    this.todoIdentifier = todoIdentifier;
  }

  public boolean isEmpty() {
    return getTodoTitle().equals("");
  }

  public Date getTodoCreated() {
    return todoCreated;
  }

  public void setTodoCreated(Date todoCreated) {
    this.todoCreated = todoCreated;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }
}
