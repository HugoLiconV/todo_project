package android.eps.uam.es.todo_project.walkthrough;

import android.content.Context;
import android.content.Intent;
import android.eps.uam.es.todo_project.MainActivity;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.eps.uam.es.todo_project.R;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Walkthrough extends AppCompatActivity {

  private ViewPager viewPager;
  private IntroManager introManager;
  private LinearLayout dotsLayout;
  private TextView[] dots;
  private int[] layouts;
  private Button btnSkip, btnNext;
  private MyViewPagerAdapter myViewPagerAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

//    Revisa si es la primera vez que se lanza la app, antes de llamar setContentView()
    introManager = new IntroManager(this);
    if (!introManager.isFirstTimeLaunch()) {
      launchHomeScreen();
      finish();
    }

    // Hace transparente la toolbar
    if (Build.VERSION.SDK_INT >= 21) {
      getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }
    setContentView(R.layout.activity_walkthrough);

    viewPager = (ViewPager) findViewById(R.id.view_pager);
    dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
    btnSkip = (Button) findViewById(R.id.btn_skip);
    btnNext = (Button) findViewById(R.id.btn_next);

    //Layouts de bienvenida
    layouts = new int[]{
        R.layout.activity_walkthrough_1,
        R.layout.activity_walkthrough_2,
        R.layout.activity_walkthrough_3,
        R.layout.activity_walkthrough_4,
        R.layout.activity_walkthrough_5,
    };

    addBottomDots(0);
    changeStatusBarColor();

    initializePageAdapter();

    btnSkip.setOnClickListener(v -> launchHomeScreen() );

    btnNext.setOnClickListener((v) -> {
      Toast.makeText(this, "funciona, plis :v", Toast.LENGTH_SHORT).show();
      //revisa si es la ultima pagina
      //si lo es la activity principal se lanza
      int current = getItem(+1);
      if (current < layouts.length) {
        viewPager.setCurrentItem(current);
      } else {
        launchHomeScreen();
      }
    });
  }

  /**
   * Hace la barra de notificaciones transparente
   */
  private void changeStatusBarColor() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Window window = getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      window.setStatusBarColor(Color.TRANSPARENT);
    }
  }

  private void launchHomeScreen() {
    introManager.setFirstTimeLaunch(false);
    Intent intent = new Intent(this, MainActivity.class);
    startActivity(intent);
  }

  private void initializePageAdapter(){
    myViewPagerAdapter = new MyViewPagerAdapter();
    viewPager.setAdapter(myViewPagerAdapter);
    viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
  }

  private void addBottomDots(int currentPage) {
    dots = new TextView[layouts.length];

    int[] colorsActive = getResources().getIntArray(R.array.dot_active);
    int[] colorsInactive = getResources().getIntArray(R.array.dot_inactive);

    dotsLayout.removeAllViews();
    for (int i = 0; i < dots.length; i++) {
      dots[i] = new TextView(this);
      dots[i].setText(Html.fromHtml("&#8226;"));
      dots[i].setTextSize(35);
      dots[i].setTextColor(colorsInactive[currentPage]);
      dotsLayout.addView(dots[i]);
    }

    if (dots.length > 0)
      dots[currentPage].setTextColor(colorsActive[currentPage]);
  }

  private int getItem(int i) {
    return viewPager.getCurrentItem() + 1;
  }

  ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

    @Override
    public void onPageSelected(int position) {
      addBottomDots(position);

      //Ultima pagina
      if (position == layouts.length - 1) {
        btnNext.setText("OK");
        btnSkip.setVisibility(View.GONE);
      } else {
        // still pages are left
        btnNext.setText("Next");
        btnSkip.setVisibility(View.VISIBLE);
      }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
  };



  public class MyViewPagerAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;

    public MyViewPagerAdapter() {
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
      layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

      View view = layoutInflater.inflate(layouts[position], container, false);
      container.addView(view);

      return view;
    }

    @Override
    public int getCount() {
      return layouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
      return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      View view = (View) object;
      container.removeView(view);
    }
  }
}
