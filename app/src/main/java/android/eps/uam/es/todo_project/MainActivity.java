package android.eps.uam.es.todo_project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.eps.uam.es.todo_project.adapter.TaskAdapter;
import android.eps.uam.es.todo_project.knapsack.Mochila;
import android.eps.uam.es.todo_project.local_storage.DataBaseService;
import android.eps.uam.es.todo_project.models.RealmLocalTask;
import android.eps.uam.es.todo_project.models.Task;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements TaskAdapter.OnReload {

  Toolbar myToolbar;
  private TaskAdapter mAdapter;
  Realm mRealm;
  RecyclerView recyclerView;
  DataBaseService dataBaseService;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
//    Inicializacion del toolbar

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    myToolbar = (Toolbar) findViewById(R.id.toolbar);
    initToolbar();

    //Inicializacion de la instancia de Realm
    mRealm = Realm.getDefaultInstance();
    initializeAdapter();
    dataBaseService = new DataBaseService();

    //permite deslizar los items del recycler view para poder eliminarlos
    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
      @Override
      public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder target) {
        return false;
      }

      @Override
      public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
        int swipedPosition = viewHolder.getAdapterPosition();
        deletFromDatabase(swipedPosition);
        Toast.makeText(MainActivity.this,
            "SwipeDir " + swipeDir + " view Holder: " + viewHolder.getItemId(), Toast.LENGTH_SHORT)
            .show();
      }
    };

    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
    itemTouchHelper.attachToRecyclerView(recyclerView);

    //changeStatusBarColor();
/*    findViewById(R.id.btn_play_again).setOnClickListener((v) -> {
      IntroManager introManager = new IntroManager(this);
      introManager.setFirstTimeLaunch(true);
      startActivity(new Intent(MainActivity.this, Walkthrough.class));
      finish();
    });*/

  }

  /**
   * Metodo para eliminar un elemento de la base de datos
   * @param position posicion del elemento a eliminar
   */
  public void deletFromDatabase(int position) {
    long id = mAdapter.getList().get(position).getTodoIdentifier();
    // obtain the results of a query
    final RealmResults<RealmLocalTask> results = mRealm
        .where(RealmLocalTask.class)
        .equalTo("id", id)
        .findAll();
    // All changes to data must happen in a transaction
    mRealm.executeTransaction(realm -> {
      results.deleteFromRealm(0);
    });
    onReload();
  }

  /**
   * metodo para inicializar el toolbar
   */
  private void initToolbar() {
    setSupportActionBar(myToolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  /**
   * Método para agregar las opciones al appbar
   */
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.navigation_main_activity, menu);
    MenuItem searchItem = menu.findItem(R.id.navigation_search);
    SearchView searchView =
        (SearchView) MenuItemCompat.getActionView(searchItem);
    searchView.setImeOptions(3);
    // Configure the search info and add any event listeners...
    return super.onCreateOptionsMenu(menu);
  }

  /**
   * Método para seleccionar los eventos del appbar
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.navigation_add:
        startActivity(new Intent(this, AddTask.class));
        Toast.makeText(this, "Adding...", Toast.LENGTH_SHORT).show();
        return true;
      case R.id.navigation_search:
        Toast.makeText(this, "Searching...", Toast.LENGTH_SHORT).show();
        return true;
      case R.id.navigation_sort:
        showSortDialog();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }

  }

  /**
   * Metodo para modificar una tarea que ha sido seleccionada por
   * el algoritm.
   * @param task
   */
  private void saveSelectedTask(Task task) {
    mRealm.executeTransaction(realm -> {
      task.setSelected(true);
      RealmLocalTask localTask = new RealmLocalTask(task);
      realm.copyToRealmOrUpdate(localTask);
    });
  }

  /**
   * Limpia todas las tareas seleccionadas por el algoritmos,
   * es decir cambia la propiedad de isSelected a false
   * esto se utiliza cuando se quiere ordenar las tareas de
   * otra forma que no sea por el algoritmo (fecha, priorirdad, tiempo)
   * @param tasks Lista de tareas a modificar
   */
  private void clearSelectedTask(final List<Task> tasks) {
    mRealm.executeTransaction(realm -> {
      for (Task task : tasks) {
        task.setSelected(false);
        RealmLocalTask localTask = new RealmLocalTask(task);
        realm.copyToRealmOrUpdate(localTask);
      }
    });
  }

  /**
   * Metodo para mostrar el dialogo que permite ordenar las tareas
   *
   */
  private void showSortDialog() {
    AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
    View mView = getLayoutInflater().inflate(R.layout.dialog_sort, null);
    mBuilder.setView(mView);
    final AlertDialog dialog = mBuilder.create();
//    Inicializacion de los elementos del dialogo
    final SeekBar seekBarTime = (SeekBar) mView.findViewById(R.id.seekBarTimeDialog);
    final RadioGroup groupNameDialog = (RadioGroup) mView.findViewById(R.id.groupNameDialog);
    final RadioGroup groupPriorityDialog = (RadioGroup) mView
        .findViewById(R.id.groupPriorityDialog);
    final RadioGroup groupDateDialog = (RadioGroup) mView.findViewById(R.id.groupDateDialog);
    final TextView lblSelectedTimeDialog = (TextView) mView
        .findViewById(R.id.lblSelectedTimeDialog);
    final Button btnKnapsackOrderDialog = (Button) mView.findViewById(R.id.btnKnapsackOrderDialog);
    seekBarTime.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        lblSelectedTimeDialog.setText(progress + "");
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {

      }
    });

    OnCheckedChangeListener mCheckListener = (group, checkedId) -> {
      if (group == groupNameDialog && !cleaning) {
        cleaning = true;
        groupDateDialog.clearCheck();
        groupPriorityDialog.clearCheck();
        cleaning = false;
      } else if (group == groupDateDialog && !cleaning) {
        cleaning = true;
        groupNameDialog.clearCheck();
        groupPriorityDialog.clearCheck();
        cleaning = false;
      } else if (group == groupPriorityDialog && !cleaning) {
        cleaning = true;
        groupNameDialog.clearCheck();
        groupDateDialog.clearCheck();
        cleaning = false;
      }

      switch (checkedId) {
        case R.id.radioNameAsc:
          sortMode = NAME_ASC;
          break;
        case R.id.radioNameDesc:
          sortMode = NAME_DESC;
          break;
        case R.id.radioPriorityAsc:
          sortMode = PRIORITY_ASC;
          break;
        case R.id.radioPriorityDesc:
          sortMode = PRIORITY_DESC;
          break;
        case R.id.radioDateAsc:
          sortMode = DATE_ASC;
          break;
        case R.id.radioDateDesc:
          sortMode = DATE_DESC;
          break;
      }

      clearSelectedTask(getTaskList());
      dataBaseService.SaveSortState(MainActivity.this, sortMode);
      onReload();
      dialog.dismiss();

    };
    groupDateDialog.setOnCheckedChangeListener(mCheckListener);
    groupNameDialog.setOnCheckedChangeListener(mCheckListener);
    groupPriorityDialog.setOnCheckedChangeListener(mCheckListener);

    btnKnapsackOrderDialog.setOnClickListener((v) -> {
      totalTime = Integer.parseInt(lblSelectedTimeDialog.getText().toString());
      solveKnapsack(totalTime);
      dialog.dismiss();
    });
    dialog.show();
  }
  private int totalTime = 0;

  /**
   * Metodo para llamar al algoritmo y modificar aquellas tareas que sean
   * seleccionadas por este.
   * @param totalTime Tiempo disponible seleccionado por el slider
   * dentro del dialogo.
   */
  private void solveKnapsack(int totalTime){
    clearSelectedTask(getTaskList());
    Mochila mochila = new Mochila(totalTime, getTaskList());
    mochila.solve();
    ArrayList<Integer> posiciones = mochila.getSelectedPositions();
    for (int i = 0; i < posiciones.size(); i++) {
      int posicion = posiciones.get(i);
      if (posicion == 1) {
        saveSelectedTask(getTaskList().get(i));
      }
    }
    sortMode = KNAPSACK;
    dataBaseService.SaveSortState(MainActivity.this, sortMode);
    onReload();
  }

  /**
   * Variables que controlan el tipo de sort de las tareas
   */
  private int sortMode = 0;//organiza las tareas por fecha de creacion
  private final int NAME_ASC = 1;
  private final int NAME_DESC = 2;
  private final int PRIORITY_ASC = 3;
  private final int PRIORITY_DESC = 4;
  private final int DATE_ASC = 5;
  private final int DATE_DESC = 6;
  private final int KNAPSACK = 7;
  boolean cleaning = false;


  /**
   * Obtiene las tareas de la base de datos, el ordenamiento se realiza desde aqui
   * @return Lista de tareas
   */
  public List<Task> getTaskList() {
    RealmResults<RealmLocalTask> result = mRealm.where(RealmLocalTask.class).findAllAsync();
    result.load();
    List<Task> tempTask = RealmLocalTask.getTaskList(result);
    //Collections.sort(tempTask, (o1, o2) -> o1.getTodoTitle().compareTo(o2.getTodoTitle()));
    switch (sortMode) {
      case NAME_ASC:
        Collections.sort(tempTask, (o1, o2) -> o1.getTodoTitle().compareTo(o2.getTodoTitle()));
        break;
      case NAME_DESC:
        Collections.sort(tempTask, (o1, o2) -> o2.getTodoTitle().compareTo(o1.getTodoTitle()));
        break;
      case PRIORITY_ASC:
        Collections.sort(tempTask, (o1, o2) -> o2.getTodoPriority() - (o1.getTodoPriority()));
        break;
      case PRIORITY_DESC:
        Collections.sort(tempTask, (o1, o2) -> o1.getTodoPriority() - (o2.getTodoPriority()));
        break;
      case DATE_ASC:
        Collections.sort(tempTask, (o1, o2) -> o2.getTodoDate().compareTo(o1.getTodoDate()));
        break;
      case DATE_DESC:
        Collections.sort(tempTask, (o1, o2) -> o1.getTodoDate().compareTo(o2.getTodoDate()));
        break;
      case KNAPSACK:
        Collections.sort(tempTask, (o1, o2) -> Boolean.compare(o1.isSelected(), o2.isSelected()));
      default:
        Collections.sort(tempTask, (o1, o2) -> o1.getTodoCreated().compareTo(o2.getTodoCreated()));
        break;
    }
    return tempTask;
  }

  /**
   * Inicializacion del custom adapter
   */
  private void initializeAdapter() {
    mAdapter = new TaskAdapter(this);
    recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(mAdapter);
  }


  @Override
  protected void onResume() {
    super.onResume();
    onReload();
  }

  /**
   * Interfaz para recargar la base de datos
   * en caso de un cambio.
   */
  @Override
  public void onReload() {
    mAdapter.setTaskList(getTaskList());
    mAdapter.notifyDataSetChanged();
    sortMode = dataBaseService.getSortState(MainActivity.this);
  }
}
