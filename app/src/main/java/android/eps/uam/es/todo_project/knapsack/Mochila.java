package android.eps.uam.es.todo_project.knapsack;

import android.eps.uam.es.todo_project.models.Task;
import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hugo on 04/06/17.
 */

/**
 * Clase para el algoritmo knapsack
 */
public class Mochila {

  private int totalTime;
  private List<Task> tasks;
  //Lista para almacenar los ids de las tareas seleccionadas por el algoritmo
  //lista para almacenar las posiciones de los elementos seleccionados
  ArrayList<Integer> selected = new ArrayList<>();

  public Mochila(int totalTime, List<Task> tasks) {
    this.totalTime = totalTime;
    this.tasks = tasks;
  }

  final String TAG = "MOCHILA";

  public void solve() {
    int NEGATIVE_INFINITY = Integer.MIN_VALUE;
    int totalElements = tasks.size();
    int[][] m = new int[totalElements + 1][totalTime + 1];
    int[][] sol = new int[totalElements + 1][totalTime + 1];

    for (int i = 1; i <= totalElements; i++) {
      Task actualTask = tasks.get(i-1);
      for (int j = 0; j <= totalTime; j++) {
        int m1 = m[i - 1][j];
        int m2 = NEGATIVE_INFINITY;
        if (j >= actualTask.getTodoTime()) {
          m2 = m[i - 1][j - actualTask.getTodoTime()] + actualTask.getTodoPriority();
        }
        /** selecciona los maximos entre m1 o m2**/
        m[i][j] = Math.max(m1, m2);
        sol[i][j] = m2 > m1 ? 1 : 0;
      }
    }
    /** Se hace la lista de los elementos seleccionados**/
    for (int n = totalElements, w = totalTime; n > 0; n--) {
      if (sol[n][w] != 0) {
        selected.add(1);
        //ids[n] = tasks.get(n-1).getTodoIdentifier();
        w = w - tasks.get(n-1).getTodoTime();
      } else {
        selected.add(0);
      }
    }
    /** Impression de los elementos seleccionados **/
    Log.e(TAG,"\nItems selected : ");
    for (int i = 0; i < totalElements; i++) {
      if (selected.get(i) == 1) {
        Log.e(TAG, i + " ");
      }
    }

    System.out.println();
  }



  public ArrayList<Integer> getSelectedPositions(){
    ArrayList<Integer> tempElements = new ArrayList<>(selected);
    Collections.reverse(tempElements);
    return tempElements;
  }
}
