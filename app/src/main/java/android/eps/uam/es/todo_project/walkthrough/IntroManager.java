package android.eps.uam.es.todo_project.walkthrough;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Hugo on 07/05/17.
 */

/**
 * Clase para guardar que utiliza shared preference
 * para detectar si es la primera vez que se ejecuta la App,
 * esto para mostrar un mensaje de bienvenida
 */
public class IntroManager {
  SharedPreferences pref;
  SharedPreferences.Editor editor;
  Context context;

  private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

  public IntroManager(Context context){
    this.context = context;
    pref = context.getSharedPreferences("IS_FIRST_TIME_LAUNCH", Context.MODE_PRIVATE);
    editor = pref.edit();
  }

  public void setFirstTimeLaunch(boolean isFirstTime) {
    editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
    editor.commit();
  }

  public boolean isFirstTimeLaunch() {
    return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
  }
}
