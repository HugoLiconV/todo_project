package android.eps.uam.es.todo_project.local_storage;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.eps.uam.es.todo_project.models.RealmLocalTask;
import android.eps.uam.es.todo_project.models.Task;
import android.view.View;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import java.util.List;

/**
 * Created by Hugo on 21/05/17.
 */

public class DataBaseService extends Application {

  private static final String SORT_STATE = "SHARED_PREFERENCES_SORT";

  @Override
  public void onCreate() {
    super.onCreate();
    Realm.init(this);
    RealmConfiguration config = new RealmConfiguration.Builder()
        .deleteRealmIfMigrationNeeded()
        .build();
    Realm.setDefaultConfiguration(config);
  }


  public void SaveSortState(Context context, int state) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(SORT_STATE, 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.putInt(SORT_STATE, state);
    editor.commit();
  }

  public int getSortState(Context context) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(SORT_STATE, 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    return sharedPreferences.getInt(SORT_STATE, 0);
  }

/*  public void SaveTaks(final List<Task> tasks) {
    mRealm.executeTransaction(realm -> {
      for (Task task : tasks) {
        RealmLocalTask localTask = new RealmLocalTask(task);
        realm.copyToRealmOrUpdate(localTask);
      }
//                realm.commitTransaction();
    });
  }*/

/*  public void deletFromDatabase(long id) {
    // obtain the results of a query
    final RealmResults<RealmLocalTask> results = mRealm.where(RealmLocalTask.class).equalTo("id", id)
        .findAll();

  // All changes to data must happen in a transaction
    mRealm.executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
*//*        // remove single match
        results.deleteFirstFromRealm();
        results.deleteLastFromRealm();

        // remove a single object
        Person person = results.get(5);*//*
        results.deleteFromRealm(0);

        // Delete all matches
        //results.deleteAllFromRealm();
      }
    });
  }*/
}
